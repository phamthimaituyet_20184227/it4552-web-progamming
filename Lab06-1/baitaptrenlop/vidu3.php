<html>
<head>
    <title>Create Table</title>
</head>
<body>
<?php
$server = 'localhost';
$user = 'root';
$pass = '';
$mydb = 'mydatabase';
$table_name = 'products';
$connect = new mysqli($server, $user, $pass,$mydb);
if ($connect->connect_error) {
    die ("Cannot connect to $server using $user");
} 

print '<font size="5" color="blue">';
print "$table_name Data</font><br>";
$sql =  "SELECT * FROM $table_name";

print "The query is <i>$sql </i><br>";
$result = $connect->query($sql);
if ($result->num_rows > 0) {
    print '<table border=1>';
    print '<th>Num<th>Product<th>Cost<th>Weight<th>Count';
    while ($row = $result->fetch_assoc()){
        print '<tr>';
        foreach ($row as $field) {
        print "<td>$field</td> ";
        }
        print '</tr>';
    }
} else { die ("Query=$sql failed!"); }


$connect->close();
?>

</body>
</html>