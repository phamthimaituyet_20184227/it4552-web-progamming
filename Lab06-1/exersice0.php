<?php
    include "./connect.php";

    $table1 = "Businesses";
    $table2 = "Biz_Categories";
    $table3 = "Categories";

    $SQLcmd1 = "CREATE TABLE $table1 (
        BusinessID VARCHAR(20),
        Name VARCHAR(50),
        Address VARCHAR(50),
        City VARCHAR(20),
        Telephone INT,
        URL VARCHAR(100),
        PRIMARY KEY(BusinessID)
        )";

    $SQLcmd2 = "CREATE TABLE $table2 (
        BusinessID VARCHAR(20),
        CategoryID VARCHAR(20),
        PRIMARY KEY(BusinessID,CategoryID)
        )";
    
    $SQLcmd3 = "CREATE TABLE $table3 (
        CategoryID VARCHAR(20),
        Title VARCHAR(50),
        Description VARCHAR(100),
        PRIMARY KEY(CategoryID)
        )";
    
    if ($connect->query($SQLcmd1) && $connect->query($SQLcmd2) && $connect->query($SQLcmd3)) {
        print '<font size="4" color="blue" >Created Table Thanh cong';
    } else {
        echo $connect->error;
    }
    $connect->close();
?>