<html>
<head>
    <title>Business Listings</title>
</head>
<body>
     <?php
        include "./connect.php"; 
     ?>
    <h1>Business Listings</h1>

    <div style="float:left; width:30%">
        <table style="border: 1px;">
            <tr>
                <th>Click on a category to find business listings</th>
            </tr>

            <?php

            $SQLcmd = "SELECT * FROM Categories";
            $result = mysqli_query($connect, $SQLcmd);

            if ($result) {
                while ($row = $result->fetch_assoc()) {
                    $link = "Business_list.php?CategoryID={$row["CategoryID"]}";
                    print("<tr><td><a href='$link'>" . $row["Title"] . "</a></td></tr>");
                }
            } else {
                print("Result empty <br>");
            }



            ?>
        </table>
    </div>
    <div>
        <table style="border: 1px;">
            <?php
            if (array_key_exists("CategoryID", $_GET)) {
                $cat_id = $_GET["CategoryID"];
                $query = "SELECT * FROM businesses JOIN biz_categories ON businesses.BusinessID = biz_categories.BusinessID
                        WHERE biz_categories.CategoryID = '$cat_id'";
                $result = $connect->query($query);
                if ($result) {
                    while ($row = $result->fetch_assoc()) {
                        echo "<tr>";
                        echo "<td>" . $row['BusinessID'] . "</td>";
                        echo "<td>" . $row['Name'] . "</td>";
                        echo "<td>" . $row['Address'] . "</td>";
                        echo "<td>" . $row['City'] . "</td>";
                        echo "<td>" . $row['Telephone'] . "</td>";
                        echo "<td>" . $row['URL'] . "</td>";
                        echo "</tr>";
                    }
                }
            }
            ?>
        </table>
    </div>
</body>

</html>