<?php
    class BaseClass {
        protected $name = "BaseClass";
        function __construct(){
            print ("In " . $this->name . "constructor<br>");
        }
        function __destruct()
        {
            print ("Destroying " . $this->name . "<br>");
        }
    }
    class SubClass extends BaseClass{
        function __construct(){
            parent:: __construct();
            print "In subClass constructor\n";
        }
        function __destruct(){
            parent:: __destruct();
        }
    }
    $obj = new SubClass;    
    $obj = new BaseClass;
   
?>
