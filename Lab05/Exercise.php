<?php
    class Page{
        private $page = "<h1>Welcome to my web page</h1><br>";
        private $title;
        private $year;
        private $copyright;

        private function addHeader(){
            $this->page = $this->page . "<p>Title : $this->title </p><br>";
        }
        public function addContent($content){
            $this ->addHeader();
            $this->page = $this->page . $content ."<br>";
            $this -> addFooter();
        }
        private function addFooter(){
            $this->page = $this->page . "Copyright  $this->copyright at $this->year year<br>";
        }
        public function get(){
            print("$this->page");
        }
        public function setTitle($title){
            $this->title = $title;
        }
        public function setYear($year){
            $this->year = $year;
        }
        public function setCopyright($copyright){
            $this->copyright = $copyright;
        }

    }
    $obj = new Page;
    $obj->setTitle("Web page");
    $obj->setCopyright("Mai Tuyet");
    $obj->setYear("2021");
    $obj->addContent("This is content");
    $obj->get();
?>