<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>DateTimeProcessing</title>
</head>
<body style="text-align: center;">
    <p>Enter your name and select date and time for the appointment</p>
    <form action="" method="post">
        Your name : <input type="text" name="name"><br><br>
        <?php
            print("Date : ");
            echo ("&emsp;&emsp;");
            print("<select  name = \"date\">");
            for($i = 1; $i<=31; $i++){
                print("<option>$i</option>");
            }
            print("</select>");
            echo ("&emsp;");
            print("<select name = \"thang\">");
            for($i = 1; $i <= 12; $i++){
                print("<option>$i</option>");
            }
            print("</select>");
            echo ("&emsp;");
            print("<select name = \"nam\">");
            for($i = 2000; $i <= 3000; $i++){
                print("<option>$i</option>");
            }
            print("</select>");
            print("<br><br>Time : ");
            echo ("&emsp;&emsp;");
            print("<select  name = \"gio\">");
            for($i = 1; $i<=24; $i++){
                print("<option>$i</option>");
            }
            print("</select>");
            echo ("&emsp;");
            print("<select name = \"phut\">");
            for($i = 0; $i <= 59; $i++){
                print("<option>$i</option>");
            }
            print("</select>");
            echo ("&emsp;");
            print("<select name = \"giay\">");
            for($i = 0; $i <= 59; $i++){
                print("<option>$i</option>");
            }
            print("</select>");
            
        ?>
        <br><br><input type="submit" name="submit" value="submit">
        &emsp13;
        <input type="submit" name="restart" value="restart">
    </form><br>
    <?php
    if(isset($_POST['submit'])){
        $name = $date = $thang = $nam = $gio = $phut = $giay = "";
        if(isset($_POST['name']) == NULL){
            echo "Nhap ten cua ban!";
        }else{
            $name = $_POST['name'];
            if(isset($_POST['date'])){
                $date = $_POST['date'];
            }
            if(isset($_POST['thang'])){
                $thang = $_POST['thang'];
            }
            if(isset($_POST['nam'])){
                $nam = $_POST['nam'];
            }
            if(isset($_POST['gio'])){
                $gio = $_POST['gio'];
            }
            if(isset($_POST['phut'])){
                $phut = $_POST['phut'];
            }
            if(isset($_POST['giay'])){
                $giay = $_POST['giay'];
            }
            echo "Hi $name!";
            print("<br>");
            echo "You have choose to have an appoinment on $gio:$phut:$giay, $date/$thang/$nam";
            echo "<br><br>";
            echo "More information";
            echo "<br><br>";
            $a = "";
            if($gio >= 1 && $gio <=12){
                $a = "AM";
            }else{
                $a = "PM";
            }
            switch($gio){
                case 13:
                    $gio = 1;
                    break;
                case 14:
                    $gio = 2;
                    break;
                case 15:
                    $gio = 3;
                    break;
                case 16:
                    $gio = 4;
                    break;
                case 17:
                    $gio = 5;
                    break;
                case 18:
                    $gio = 6;
                    break;
                case 19:
                    $gio = 7;
                    break;
                case 20:
                    $gio = 8;
                    break;
                case 21:
                    $gio = 9;
                    break;
                case 22:
                    $gio = 10;
                    break;
                case 23:
                    $gio = 11;
                    break;
                case 24:
                    $gio = 0;
                    break;
                default:
                break;
            }
            echo "In 12 hours, the time and date is $gio:$phut:$giay $a, $date/$thang/$nam";
            echo "<br><br>";
            if($nam%4 == 0 || $nam%100 == 0 && $nam%400 == 0){
                switch($thang){
                    case 1: case 3 : case 5: case 7: case 8: case 10: case 12:
                        $songay = 31;
                        break;
                    case 2:
                        $songay = 29;
                        break;
                    case 4: case 6: case 9: case 11:
                        $songay = 30;
                        break;
                }   
            }else{
                switch($thang){
                    case 1: case 3 : case 5: case 7: case 8: case 10: case 12:
                        $songay = 31;
                        break;
                    case 2:
                        $songay = 28;
                        break;
                    case 4: case 6: case 9: case 11:
                        $songay = 30;
                        break;
                }
            }      
            echo "This month has $songay days!";
        }
    }
?>
</body>
</html>