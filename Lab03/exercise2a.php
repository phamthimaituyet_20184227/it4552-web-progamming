<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Convert radians to degrees and vice versa</title>
</head>
<body style="text-align: center;">
    <?php
        // chuyen sang do 
        function chuyen_do($rad){
            $do = $rad * 180/3.14;
            return($do);
        }
        // chuyen sang radian
        function chuyen_rad($do){
            $rad = (3.14/180)*$do;
            return $rad;
        }
    ?>
    <form action="" method="post">
        Nhap so muon chuyen : <input type="text" name="so"><br><br>
        <input type="radio" name="chuyen" value="chuyen_do">Chuyen tu radian sang do
        <input type="radio" name="chuyen" value="chuyen_radian">Chuyen tu do sang radian
        <p>Ket qua : 
            <?php
                if(isset($_POST['so'])){
                    $so = $_POST['so'];
                    if(isset($_POST['chuyen'])){
                        $chuyen = $_POST['chuyen'];
                       
                        if($chuyen == 'chuyen_do'){
                            $so = chuyen_do($so);
                            echo $so;
                        }
                        if($chuyen == 'chuyen_radian'){
                            $so = chuyen_rad($so);
                            
                            echo $so;
                        }
                    }
                }   
            ?>
        </p>
        <button type="submit">Submit</button>
    </form>
    
</body>
</html>