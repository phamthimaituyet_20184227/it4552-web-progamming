<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>MessagesBoard</title>
</head>
<body style="align-items: center;">
    <?php
        if(isset($_POST['submit'])){
            if(isset($_POST['name']) && isset($_POST['comment'])){
                $name = $_POST['name'];
                $cmt = $_POST['comment'];
                $file = fopen('./posts/post_'.date('YmdHis').'.txt','w');
                $txt = $name.' says: '.$cmt;
                fwrite($file,$txt);
            }else{
                echo "Lỗi chưa đăng nhập đủ thông tin";
            }
        }

        if (isset($_POST['submit']))
        {
            // Nếu người dùng có chọn file để upload
            if ($_FILES['anh']['name'] != null)
            {
                // Nếu file upload không bị lỗi,
                // Tức là thuộc tính error > 0
                if ($_FILES['anh']['error'] > 0)
                {
                    echo 'File Upload Bị Lỗi';
                }
                else{
                    // Upload file
                    $temp = explode(".", $_FILES["anh"]["name"]);
                    $newfilename = 'post_'.date('YmdHis'). '.' . end($temp);
                    move_uploaded_file($_FILES['anh']['tmp_name'], './posts/'.$newfilename);
                }
            }
        }
    ?>
    <h1>Messages Board</h1>
    
    <?php
        $readfodername = "./posts";
        $result = scandir($readfodername);           // doc toan bo file trong thu muc posts
        // var_dump($result);
        $arrayfile = array_diff($result, array('.', '..'));    // loại bỏ biến trong result 
        //var_dump($arrayfile);
        if(count($arrayfile) > 0){
            $user = [];
            $messageImg = ['message'=>null,'img'=>null];
            foreach($arrayfile as $value){
                $dinhdang = explode(".",$value);
                if($dinhdang[1] != 'txt'){
                    $messageImg['img'] = $value;
                }else{
                    $file = fopen("./posts/".$value,"r");
                    $txt = fgets($file);
                    $messageImg['message'] = $txt;
                    array_push($user,$messageImg);
                    $messageImg['img'] = null;
                }          
            }

            foreach($user as $value){
                if($value['img'] != null){
                    print '<div style="border: 1px solid black; width : 37%; background: #FFEEDD;overflow: auto">';
                    print '<img src="./posts/'.$value['img'].'" style = "width : 47% ;float: left" />';
                    print $value['message'].' </div><br>';
                }else{
                    print '<div style="border: 1px solid black; width : 37%; background: #FFEEDD;">'.$value['message'].' </div><br>';
                }
            }
           
        }else{            
            print ' <p style="font-size: 15px;">Chưa có thông điệp nào được đăng</p><br>';
        }
    ?>
    
    <form action="" method="post" enctype="multipart/form-data" >
        <fieldset style="width: 35%;">
            <legend>Đăng tải một thông điệp mới</legend>
            <p>Tên người dùng : </p>
            <input type="text" name="name" style="width: 300px;"><br><br>
            <input type="checkbox" name="no_name" />Đăng thông điệp vô danh<br><br>
            <p>Nội dung thông điệp: </p>
            <textarea name="comment"  cols="40" rows="8"></textarea><br>
            <p>Hình ảnh kèm theo (tùy chọn):</p>
            <input type="file" name="anh" ><br><br>
            <button type="submit" name="submit">Đăng thông điệp</button>
        </fieldset>
    </form>
</body>
</html>